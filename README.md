# Description Sidebar

Description Sidebar is a Redmine plugin that provides ability to embed "description" of a project to the sidebar of "issue" pages of the project.

## Install

```
% cd /path/to/redmine/plugins
% git clone https://gitlab.com/redmine-plugin-description-sidebar/redmine-plugin-description-sidebar.git description_sidebar
% cd ..
% bundle install
```

And then restart your Redmine.

## License

GPL 3 or later. See LICENSE for details.
