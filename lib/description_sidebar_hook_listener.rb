class DescriptionSidebarHookListener < Redmine::Hook::ViewListener
  def t(*args, **kwargs)
    ::I18n.t(*args, **kwargs)
  end

  def view_issues_sidebar_queries_bottom(context = {})
    if context[:controller].nil? or
       not context[:controller].is_a?(IssuesController)
      return
    end

    project = context[:project]
    return unless project

    description = project.description
    return unless description.present?

    @project = project
    content_tag("h3", l(:field_description)) + textilizable(description)
  end
end
